## Employee monthly payslip

Given a csv with employee's details: first name, last name, annual salary (positive integer) and super rate (0% - 50% inclusive), payment start date, payment_end_date, the program generates payslip information with name, pay period, gross income, income tax, net income and super.

The calculation details will be the following:
* pay period = per calendar month
* gross income = annual salary / 12 months
* income tax = based on the tax table provide below
* net income = gross income - income tax
* super = gross income x super rate

All calculation results are rounded to the whole dollar.

The following rates are used for calculating tax.
```
Taxable income          Tax on this income
0 - $18,200             Nil
$18,201 - $37,000       19c for each $1 over $18,200
$37,001 - $80,000       $3,572 plus 32.5c for each $1 over $37,000
$80,001 - $180,000      $17,547 plus 37c for each $1 over $80,000
$180,001 and over       $54,547 plus 45c for each $1 over $180,000
```
### Example Data
Employee annual salary is 60,050, super rate is 9%, how much will this employee be paid for the month of March ?
* pay period = Month of March (01 March to 31 March)
* gross income = 60,050 / 12 = 5,004.16666667 (round down) = 5,004
* income tax = (3,572 + (60,050 - 37,000) x 0.325) / 12  = 921.9375 (round up) = 922
* net income = 5,004 - 922 = 4,082
* super = 5,004 x 9% = 450.36 (round down) = 450

Here is the csv input and output format.

```
Input (first name, last name, annual salary, super rate (%), payment start date):
David,Rudd,60050,9%,01 March - 31 March
Ryan,Chen,120000,10%,01 March - 31 March

Output (name, pay period, gross income, income tax, net income, super):
David Rudd, 2016/03/01 - 2016/03/31, 5004, 922, 4082, 450
Ryan Chen, 2016/03/01 - 2016/03/31, 10000, 2696, 7304, 1000
```

When the input payment start and end date span multiple months we will generate multiple rows with payslip information, split per month.

### Usage

#### Requirements

- Ruby 2.3
- `bundle install` all the gems

#### Running

`bin/payslips` to read from stdin and write to stdout
`bin/payslips -i <input_file> -o <output_file>` to provide input or output files

eg. `bin/payslips -i spec/support/etl_input.csv -o output.csv`

#### Testing

`rspec` to run the tests
