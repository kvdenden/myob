class PayslipRepresenter
  attr_reader :payslip
  def initialize(payslip)
    @payslip = payslip
  end

  def to_s
    [
      payslip.name,
      format_pay_period(payslip.pay_period),
      payslip.gross_income,
      payslip.income_tax,
      payslip.net_income,
      payslip.superannuation
    ].join(', ')
  end

  private

  def format_pay_period(pay_period)
    [format_date(pay_period.start_date), format_date(pay_period.end_date)].join(' - ')
  end

  def format_date(date)
    date.strftime('%Y/%m/%d')
  end
end
