class SalaryValidator
  include Veto.validator

  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :amount, presence: true, greater_than_or_equal_to: 0, integer: true
  validates :super_rate, presence: true, greater_than_or_equal_to: 0, less_than_or_equal_to: 50
  validates :payment_start_date, presence: true
end
