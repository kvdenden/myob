class TaxCalculator
  class << self
    DEFAULT_TAX_BRACKETS = { 18_200 => 0.19, 37_000 => 0.325, 80_000 => 0.37, 180_000 => 0.45 }.freeze
    def call(amount)
      new(tax_brackets: DEFAULT_TAX_BRACKETS).call(amount)
    end
  end

  def initialize(tax_brackets:)
    @tax_brackets = tax_brackets
  end

  def call(amount)
    current_bracket = find_tax_bracket(amount)
    if current_bracket.positive?
      (amount - current_bracket) * tax_brackets[current_bracket] + call(current_bracket)
    else
      0
    end
  end

  protected

  attr_reader :tax_brackets

  def find_tax_bracket(amount)
    tax_brackets.keys.sort.reverse.find { |b| b < amount } || 0
  end
end
