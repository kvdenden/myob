class Payslip
  include Anima.new(:name, :pay_period, :gross_income, :income_tax, :superannuation)

  def net_income
    gross_income - income_tax
  end
end
