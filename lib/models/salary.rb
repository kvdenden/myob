class Salary
  include Anima.new(:first_name, :last_name, :amount, :super_rate, :payment_start_date, :payment_end_date)
  include Veto.model(SalaryValidator.new)
end
