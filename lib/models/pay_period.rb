class PayPeriod
  class << self
    def create(start_date:, end_date: nil)
      end_date ||= start_date.end_of_month
      periods = []
      while start_date < end_date
        periods << new(start_date: start_date, end_date: [start_date.end_of_month, end_date].min)
        start_date = start_date.next_month.beginning_of_month
      end
      periods
    end
  end
  attr_reader :start_date, :end_date

  def initialize(start_date:, end_date:)
    validate!(start_date, end_date)
    @start_date = start_date
    @end_date = end_date
  end

  def prorate(yearly_amount)
    monthly_amount = yearly_amount / 12.0
    days_in_month = start_date.end_of_month.day
    daily_amount = monthly_amount / days_in_month
    daily_amount * number_of_days
  end

  def ==(other)
    other.class == self.class && other.state == state
  end

  protected

  def state
    [start_date, end_date]
  end

  private

  def number_of_days
    (start_date..end_date).count
  end

  def validate!(start_date, end_date)
    raise ArgumentError, 'Start and end date must be in the same month' if invalid_period?(start_date, end_date)
  end

  def invalid_period?(start_date, end_date)
    start_date.year != end_date.year || start_date.month != end_date.month
  end
end
