class WrapWithRepresenters
  def call(payslip)
    PayslipRepresenter.new(payslip)
  end
end
