class SplitInPayPeriods
  def call(salary)
    pay_periods = PayPeriod.create(start_date: salary.payment_start_date, end_date: salary.payment_end_date)
    pay_periods.zip([salary].cycle)
  end
end
