class GeneratePayslips
  def call(args)
    pay_period, salary = args
    Payslip.new(
      pay_period: pay_period,
      name: name(salary),
      gross_income: gross_income(salary, pay_period),
      income_tax: income_tax(salary, pay_period),
      superannuation: superannuation(salary, pay_period)
    )
  end

  private

  def name(salary)
    "#{salary.first_name} #{salary.last_name}"
  end

  def gross_income(salary, pay_period)
    pay_period.prorate(salary.amount).round
  end

  def income_tax(salary, pay_period)
    tax = TaxCalculator.call(salary.amount)
    pay_period.prorate(tax).round
  end

  def superannuation(salary, pay_period)
    (gross_income(salary, pay_period) * salary.super_rate / 100.0).round
  end
end
