class SalaryRepository
  def initialize(input, parser:, logger: Logger.new(STDOUT))
    @input = input
    @parser = parser
    @logger = logger
  end

  def each
    salaries.each { |s| yield s }
  end

  def salaries
    @salaries ||= input.each_line.with_index(1).map do |line, index|
      begin
        salary = parser.parse(line.chomp)
        salary.validate!
        salary
      rescue StandardError => e
        @logger.error("Invalid input on line #{index}: #{e.message}")
        nil
      end
    end.compact
  end

  protected

  attr_reader :input, :parser, :logger
end
