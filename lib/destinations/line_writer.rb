class LineWriter
  def initialize(out = $stdout)
    @out = out
  end

  def call(line)
    @out.puts(line)
  end
end
