class PayslipETL
  class << self
    DEFAULT_TRANSFORMS = [
      SplitInPayPeriods.new,
      GeneratePayslips.new,
      WrapWithRepresenters.new
    ].freeze

    def process(input: $stdin, output: $stdout)
      source ||= SalaryRepository.new(input.read, parser: SalaryParser.new)
      destination ||= LineWriter.new(output)
      new(source: source, destination: destination, transforms: DEFAULT_TRANSFORMS).process
    end
  end
  def initialize(source:, destination:, transforms:)
    @source = source
    @destination = destination
    @transforms = transforms
  end

  def process
    source.each do |el|
      result = transforms.reduce(el) do |res, transform|
        Array.wrap(res).flat_map { |r| transform.call(r) }
      end

      result.each do |res|
        destination.call(res)
      end
    end
  end

  protected

  attr_reader :source, :destination, :transforms
end
