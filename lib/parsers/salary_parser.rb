class SalaryParser
  def parse(line)
    columns = line.split(',').map(&:strip)
    Salary.new(parse_columns(columns))
  end

  private

  def parse_columns(columns)
    raise ArgumentError, columns.to_s unless columns.count == column_parsers.count
    attrs = {}
    column_parsers.zip(columns).each do |(key, parser), value|
      attrs.merge!(parse_column(parser, value, column_name: key))
    end
    attrs
  end

  def parse_column(parser, value, column_name: nil)
    if parser.is_a?(Symbol)
      { column_name => parser.to_proc.call(value) }
    else
      parser.call(value)
    end
  end

  def column_parsers
    {
      first_name: :itself,
      last_name:  :itself,
      amount:     :to_i,
      super_rate: :to_i,
      payment_start_date: ->(str) { parse_dates(str) }
    }
  end

  def parse_dates(str)
    start_date, end_date = str.split('-').map { |s| Date.parse(s.strip) }
    { payment_start_date: start_date, payment_end_date: end_date }
  end
end
