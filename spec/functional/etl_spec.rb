RSpec.describe PayslipETL do
  let(:input) { File.read('spec/support/etl_input.csv') }
  let(:expected_output) { File.read('spec/support/etl_output.csv') }
  let(:output) { StringIO.new }

  describe '::process' do
    it 'writes the expected output to destination' do
      PayslipETL.process(input: StringIO.new(input), output: output)
      expect(output.string).to eq(expected_output)
    end
  end
end
