RSpec.describe LineWriter do
  let(:out) { StringIO.new }
  subject(:writer) { LineWriter.new(out) }
  let(:line) { 'Hello world' }

  it 'writes line to output' do
    expect { writer.call(line) }.to change { out.string }.to(line + "\n")
  end
end
