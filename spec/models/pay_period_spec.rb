RSpec.describe PayPeriod do
  let(:start_date) { Date.new(2016, 8, 1) }
  let(:end_date) { Date.new(2016, 8, 31) }
  subject(:pay_period) { PayPeriod.new(start_date: start_date, end_date: end_date) }

  its(:start_date) { is_expected.to eq(start_date) }
  its(:end_date) { is_expected.to eq(end_date) }

  context 'when start and end date are not in the same month' do
    let(:end_date) { Date.new(2016, 9, 30) }
    it 'raises an exception' do
      expect { pay_period }.to raise_error(ArgumentError)
    end
  end

  describe 'prorate' do
    let(:yearly_amount) { 100_000 }
    context 'when pay period is a whole month' do
      it 'returns 1/12th of the yearly amount' do
        expect(pay_period.prorate(yearly_amount)).to be_within(0.5).of(8333.3334)
      end
    end

    context 'when pay period is a partial month' do
      let(:start_date) { Date.new(2016, 8, 18) }
      it 'returns correct prorated amount' do
        expect(pay_period.prorate(yearly_amount)).to be_within(0.5).of(3763.4409)
      end
    end
  end

  describe '::create' do
    context 'given a start date' do
      let(:start_date) { Date.new(2016, 8, 18) }
      let(:pay_periods) { PayPeriod.create(start_date: start_date) }
      it 'creates one pay period from that date until the end of the month' do
        expected = [
          PayPeriod.new(start_date: start_date, end_date: Date.new(2016, 8, 31))
        ]
        expect(pay_periods).to eq(expected)
      end
    end

    context 'given a start date and end date' do
      let(:start_date) { Date.new(2016, 8, 18) }
      let(:end_date) { Date.new(2016, 10, 20) }
      let(:pay_periods) { PayPeriod.create(start_date: start_date, end_date: end_date) }
      it 'creates an array of pay periods per month' do
        expected = [
          PayPeriod.new(start_date: start_date, end_date: Date.new(2016, 8, 31)),
          PayPeriod.new(start_date: Date.new(2016, 9, 1), end_date: Date.new(2016, 9, 30)),
          PayPeriod.new(start_date: Date.new(2016, 10, 1), end_date: end_date)
        ]
        expect(pay_periods).to eq(expected)
      end
    end
  end
end
