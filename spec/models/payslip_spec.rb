RSpec.describe Payslip do
  let(:name) { 'John Doe' }
  let(:pay_period) { double('pay period') }
  let(:gross_income) { 100 }
  let(:income_tax) { 20 }
  let(:superannuation) { 9 }
  subject(:payslip) do
    Payslip.new(
      name: name,
      pay_period: pay_period,
      gross_income: gross_income,
      income_tax: income_tax,
      superannuation: superannuation
    )
  end

  its(:name) { is_expected.to eq(name) }
  its(:pay_period) { is_expected.to eq(pay_period) }
  its(:gross_income) { is_expected.to eq(gross_income) }
  its(:income_tax) { is_expected.to eq(income_tax) }
  its(:superannuation) { is_expected.to eq(superannuation) }

  it 'calculates net income' do
    expect(payslip.net_income).to eq(gross_income - income_tax)
  end
end
