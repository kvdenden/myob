RSpec.describe Salary do
  let(:first_name) { 'John' }
  let(:last_name) { 'Doe' }
  let(:amount) { 80_000 }
  let(:super_rate) { 9 }
  let(:payment_start_date) { Date.new(2016, 8, 18) }
  let(:payment_end_date) { Date.new(2016, 8, 31) }

  subject(:salary) do
    Salary.new(
      first_name: first_name,
      last_name: last_name,
      amount: amount,
      super_rate: super_rate,
      payment_start_date: payment_start_date,
      payment_end_date: payment_end_date
    )
  end

  its(:first_name) { is_expected.to eq(first_name) }
  its(:last_name) { is_expected.to eq(last_name) }
  its(:amount) { is_expected.to eq(amount) }
  its(:super_rate) { is_expected.to eq(super_rate) }
  its(:payment_start_date) { is_expected.to eq(payment_start_date) }
  its(:payment_end_date) { is_expected.to eq(payment_end_date) }

  describe 'valid?' do
    its(:valid?) { is_expected.to be true }

    context 'without first name' do
      let(:first_name) { '' }
      its(:valid?) { is_expected.to be false }
    end

    context 'without last name' do
      let(:last_name) { '' }
      its(:valid?) { is_expected.to be false }
    end

    context 'without amount' do
      let(:amount) { nil }
      its(:valid?) { is_expected.to be false }
    end

    context 'with negative amount' do
      let(:amount) { -100 }
      its(:valid?) { is_expected.to be false }
    end

    context 'with non-integer amount' do
      let(:amount) { 1000.5 }
      its(:valid?) { is_expected.to be false }
    end

    context 'without super rate' do
      let(:super_rate) { nil }
      its(:valid?) { is_expected.to be false }
    end

    context 'with negative super rate' do
      let(:super_rate) { -10 }
      its(:valid?) { is_expected.to be false }
    end

    context 'with super rate greater than 50%' do
      let(:super_rate) { 51 }
      its(:valid?) { is_expected.to be false }
    end

    context 'without payment start date' do
      let(:payment_start_date) { nil }
      its(:valid?) { is_expected.to be false }
    end
  end
end
