RSpec.describe PayslipRepresenter do
  let(:payslip) do
    Payslip.new(
      name: 'John Doe',
      pay_period: PayPeriod.new(start_date: Date.new(2016, 8, 18), end_date: Date.new(2016, 8, 31)),
      gross_income: 1000,
      income_tax: 100,
      superannuation: 90
    )
  end

  subject(:representer) { PayslipRepresenter.new(payslip) }

  describe 'to_s' do
    it 'renders the payslip details as csv' do
      expect(representer.to_s).to eq('John Doe, 2016/08/18 - 2016/08/31, 1000, 100, 900, 90')
    end
  end
end
