RSpec.describe TaxCalculator do
  let(:tax_brackets) { { 1000 => 0.1, 2000 => 0.2 } }
  subject(:calculator) { TaxCalculator.new(tax_brackets: tax_brackets) }

  describe 'call' do
    it 'charges no tax for amounts smaller than smallest tax bracket' do
      expect(calculator.call(500)).to eq(0)
    end

    it 'charges correct amount in lowest tax bracket' do
      expect(calculator.call(1500)).to eq(50)
    end

    it 'charges correct amount in higher tax brackets' do
      expect(calculator.call(2500)).to eq(200)
    end
  end

  describe '::call' do
    it 'calculates tax using rates from July 2012' do
      expect(TaxCalculator.call(60_050)).to eq(11_063.25)
    end
  end
end
