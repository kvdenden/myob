RSpec.describe SalaryRepository do
  let(:input) { StringIO.new("FOO\nBAR") }
  let(:parser) { double('parser', parse: nil) }
  let(:logger) { double('logger', error: nil) }
  subject(:repository) { SalaryRepository.new(input, parser: parser, logger: logger) }

  describe 'salaries' do
    let(:foo) { double('foo', validate!: true) }
    let(:bar) { double('bar', validate!: true) }

    context 'parser can parse lines' do
      before do
        allow(parser).to receive(:parse).with('FOO').and_return(foo)
        allow(parser).to receive(:parse).with('BAR').and_return(bar)
      end

      it 'returns the correct number of salaries' do
        expect(repository.salaries.count).to eq(2)
      end

      it 'delegates salary creation to the parser' do
        expect(repository.salaries).to eq([foo, bar])
      end
    end

    context 'when input is invalid' do
      before do
        allow(parser).to receive(:parse).with('FOO').and_return(foo)
        allow(parser).to receive(:parse).with('BAT').and_return(bar)
      end

      context 'when parser raises error' do
        before do
          allow(parser).to receive(:parse).with('BAR').and_raise('error')
        end

        it 'returns the valid salaries' do
          expect(repository.salaries).to eq([foo])
        end

        it 'logs the error' do
          expect(logger).to receive(:error)
          repository.salaries
        end
      end

      context 'when validation raises error' do
        before do
          allow(bar).to receive(:validate!).and_raise('error')
        end

        it 'returns the valid salaries' do
          expect(repository.salaries).to eq([foo])
        end

        it 'logs the error' do
          expect(logger).to receive(:error)
          repository.salaries
        end
      end
    end
  end
end
