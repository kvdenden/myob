RSpec.describe SalaryRepository do
  let(:parser) { SalaryParser.new }

  describe 'parse' do
    subject(:result) { parser.parse(input) }
    context 'with valid input' do
      let(:input) { 'David,Rudd,60050,9%,01 March - 31 March' }
      its(:first_name) { is_expected.to eq('David') }
      its(:last_name) { is_expected.to eq('Rudd') }
      its(:amount) { is_expected.to eq(60_050) }
      its(:super_rate) { is_expected.to eq(9) }
      its(:payment_start_date) { is_expected.to eq(Date.new(Date.today.year, 3, 1)) }
      its(:payment_end_date) { is_expected.to eq(Date.new(Date.today.year, 3, 31)) }
    end

    context 'with invalid input' do
      let(:input) { 'INVALID_INPUT' }
      it 'raises an exception' do
        expect { result }.to raise_error(ArgumentError)
      end
    end

    context 'with invalid dates' do
      let(:input) { 'David,Rudd,60050,9%,INVALID_DATE' }
      it 'raises an exception' do
        expect { result }.to raise_error(ArgumentError)
      end
    end
  end
end
