RSpec.describe WrapWithRepresenters do
  let(:transform) { WrapWithRepresenters.new }

  describe 'call' do
    let(:payslip) { double('payslip') }
    subject(:representer) { transform.call(payslip) }
    it 'returns a payslip repesenter' do
      expect(representer).to be_a(PayslipRepresenter)
    end

    it 'wraps the origin payslip' do
      expect(representer.payslip).to be(payslip)
    end
  end
end
