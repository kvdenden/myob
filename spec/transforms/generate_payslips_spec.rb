RSpec.describe GeneratePayslips do
  let(:transform) { GeneratePayslips.new }
  let(:pay_period) { PayPeriod.new(start_date: Date.new(2012, 3, 1), end_date: Date.new(2012, 3, 31)) }
  let(:salary) do
    Salary.new(
      first_name: 'David',
      last_name: 'Rudd',
      amount: 60_050,
      super_rate: 9,
      payment_start_date: Date.new(2012, 3, 1),
      payment_end_date: Date.new(2012, 3, 31)
    )
  end
  describe 'call' do
    subject(:payslip) { transform.call([pay_period, salary]) }
    its(:name) { is_expected.to eq('David Rudd') }
    its(:pay_period) { is_expected.to eq(pay_period) }
    its(:gross_income) { is_expected.to eq(5004) }
    its(:income_tax) { is_expected.to eq(922) }
    its(:net_income) { is_expected.to eq(4082) }
    its(:superannuation) { is_expected.to eq(450) }
  end
end
