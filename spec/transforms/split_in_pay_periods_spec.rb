RSpec.describe SplitInPayPeriods do
  let(:transform) { SplitInPayPeriods.new }
  let(:salary) do
    Salary.new(
      first_name: 'John',
      last_name: 'Doe',
      amount: 80_000,
      super_rate: 9,
      payment_start_date: Date.new(2016, 8, 18),
      payment_end_date: Date.new(2016, 9, 20)
    )
  end

  describe 'call' do
    subject(:result) { transform.call(salary) }
    it 'returns an array with monthly pay periods' do
      expected = [
        [PayPeriod.new(start_date: Date.new(2016, 8, 18), end_date: Date.new(2016, 8, 31)), salary],
        [PayPeriod.new(start_date: Date.new(2016, 9, 1),  end_date: Date.new(2016, 9, 20)), salary]
      ]
      expect(result).to eq(expected)
    end
  end
end
