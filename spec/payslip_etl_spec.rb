RSpec.describe PayslipETL do
  let(:salary) { double('salary 1') }
  let(:transform1) { double('transform 1') }
  let(:transform2) { double('transform 2') }
  let(:payslip1) { double('payslip 1') }
  let(:payslip2) { double('payslip 2') }
  let(:destination) { double('destination') }
  subject(:etl) { PayslipETL.new(source: [salary], destination: destination, transforms: [transform1, transform2]) }

  before do
    tmp1 = double('tmp 1')
    tmp2 = double('tmp 2')
    allow(transform1).to receive(:call).with(salary).and_return([tmp1, tmp2])
    allow(transform2).to receive(:call).with(tmp1).and_return(payslip1)
    allow(transform2).to receive(:call).with(tmp2).and_return(payslip2)
  end

  describe 'process' do
    it 'calls the destination with the transformed results' do
      expect(destination).to receive(:call).with(payslip1)
      expect(destination).to receive(:call).with(payslip2)
      etl.process
    end
  end
end
