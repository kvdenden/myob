require 'bundler'

ENV['RACK_ENV'] ||= ENV.fetch('RACK_ENV', 'development')
Bundler.require(:default, ENV['RACK_ENV'].to_sym)

%w(. ./lib ./config).each do |p|
  $LOAD_PATH.unshift File.expand_path(p, File.dirname(__FILE__))
end

require 'active_support'
require 'active_support/core_ext'

require 'validators/salary_validator'
require 'models/pay_period'
require 'models/payslip'
require 'models/salary'

require 'parsers/salary_parser'
require 'repositories/salary_repository'
require 'services/tax_calculator'

require 'representers/payslip_representer'
require 'destinations/line_writer'

require 'transforms/split_in_pay_periods'
require 'transforms/generate_payslips'
require 'transforms/wrap_with_representers'

require 'payslip_etl'
